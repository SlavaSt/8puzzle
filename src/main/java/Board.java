import java.util.*;

public class Board {

    private final int[][] blocks;

    // construct a board from an N-by-N array of blocks
    public Board(int[][] blocks) {
        this.blocks = deepClone(blocks);
    }

    // (where blocks[i][j] = block in row i, column j)
    public int dimension() {
        return blocks.length;
    }                 // board dimension N

    public int hamming() {
        int counter = 0;
        int k = 1;
        for (int i = 0; i < dimension(); i++) {
            for (int j = 0; j < dimension(); j++) {
                if (blocks[i][j] != 0) {
                    Map.Entry<Integer, Integer> expectedCoords = numberToCoords(k++);
                    if (!new AbstractMap.SimpleImmutableEntry(i, j).equals(expectedCoords))
                        counter++;
                }
            }
        }
        return counter;
    }                   // number of blocks out of place

    public int manhattan() {
        int distance = 0;
        for (int i = 0; i < dimension(); i++) {
            for (int j = 0; j < dimension(); j++) {
                if (blocks[i][j] == 0) continue;
                Map.Entry<Integer, Integer> coords = numberToCoords(blocks[i][j]);
                distance += Math.abs(coords.getKey() - i);
                distance += Math.abs(coords.getValue() - j);
            }
        }
        return distance;
    }                 // sum of Manhattan distances between blocks and goal

    public boolean isGoal() {
        int k = 1;
        for (int i = 0; i < dimension(); i++) {
            for (int j = 0; j < dimension(); j++) {
                if (blocks[i][j] != k++ && (i != dimension() - 1 || j != dimension() - 1))
                    return false;
            }
        }
        return true;
    }                // is this board the goal board?

    public Board twin() {
        int[][] twin = deepClone(blocks);
        if (blocks[0][0] != 0 && blocks[0][1] != 0)
            swap(twin, 0, 0, 0, 1);
        else swap(twin, 1, 0, 1 ,1);
        return new Board(twin);
    }                    // a board obtained by exchanging two adjacent blocks in the same row

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Board)) return false;
        return Arrays.deepEquals(blocks, (((Board) o).blocks));
    }

//    @Override
//    public int hashCode() {
//        return Arrays.deepHashCode(blocks);
//    }

    public Iterable<Board> neighbors() {
        int i, j = 0;
        loop:
        for (i = 0; i < dimension(); i++) {
            for (j = 0; j < dimension(); j++) {
                if (blocks[i][j] == 0)
                    break loop;
            }
        }
        List<Board> neigbours = new ArrayList<Board>();
        int[][] blocks;
        if (isInBounds(i - 1)) {
            blocks = deepClone(this.blocks);
            swap(blocks, i - 1, j, i, j);
            neigbours.add(new Board(blocks));
        }
        if (isInBounds(i + 1)) {
            blocks = deepClone(this.blocks);
            swap(blocks, i + 1, j, i, j);
            neigbours.add(new Board(blocks));
        }
        if (isInBounds(j - 1)) {
            blocks = deepClone(this.blocks);
            swap(blocks, i, j - 1, i, j);
            neigbours.add(new Board(blocks));
        }
        if (isInBounds(j + 1)) {
            blocks = deepClone(this.blocks);
            swap(blocks, i, j + 1, i, j);
            neigbours.add(new Board(blocks));
        }
        return neigbours;
    }// all neighboring boards

    private static final String EOL = System.getProperty("line.separator");

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(dimension() + "\n");
        for (int i = 0; i < dimension(); i++) {
            for (int j = 0; j < dimension(); j++) {
                s.append(String.format("%2d ", blocks[i][j]));
            }
            s.append("\n");
        }
        return s.toString();
    }

    private final Map.Entry<Integer, Integer> numberToCoords(int i) {
        if (i == 0) return new AbstractMap.SimpleImmutableEntry<Integer, Integer>(dimension() - 1, dimension() - 1);
        int y = (i - 1) / dimension();
        int x = (i - 1) - dimension() * y;
        return new AbstractMap.SimpleImmutableEntry<Integer, Integer>(y, x);
    }

    private static final void swap(int[][] blocks, int x1, int y1, int x2, int y2) {
        int tmp = blocks[x1][y1];
        blocks[x1][y1] = blocks[x2][y2];
        blocks[x2][y2] = tmp;
    }

    private final void swap(int x1, int y1, int x2, int y2) {
        swap(blocks, x1, y1, x2, y2);
    }

    private final void safeSwap(int x1, int y1, int x2, int y2) {

    }

    private static final void safeSwap(int[][] blocks, int x1, int y1, int x2, int y2) {
        if (isInBounds(blocks, x1, y1) && isInBounds(blocks, x2, y2)) {
            swap(blocks, x1, y1, x2, y2);
        }
    }

    private static final boolean isInBounds(int[][] blocks, int coord) {
        return coord >= 0 && coord < blocks.length;
    }

    private final boolean isInBounds(int coord) {
        return isInBounds(blocks, coord);
    }


    private final boolean isInBounds(int x, int y) {
        return isInBounds(x) && isInBounds(y);
    }

    private static final boolean isInBounds(int[][] blocks, int x, int y) {
        return isInBounds(blocks, x) && isInBounds(blocks, y);
    }

    private static final int[][] deepClone(int[][] array) {
        int[][] newArray = array.clone();
        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i].clone();
        }
        return newArray;
    }
}