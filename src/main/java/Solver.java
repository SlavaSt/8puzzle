

import java.util.*;


public class Solver {

    private final List<Board> solution;

    public Solver(Board initial){
        MinPQ<RichBoard> queue1 = new MinPQ<RichBoard>();
        MinPQ<RichBoard> queue2 = new MinPQ<RichBoard>();
        queue1.insert(new RichBoard(initial, Arrays.asList(initial)));
        queue2.insert(new RichBoard(initial.twin(), Arrays.asList(initial)));
        RichBoard result1, result2;
        do {
            //System.out.println("---Queue---");
            //ImmutableList.copyOf(queue1).stream().map(x -> "moves: " + x.moves() + " " + x.delegate()).forEach(System.out::println);
            result1 = tryNeigbours(queue1);
            result2 = tryNeigbours(queue2);
        } while (result1 == null && result2 == null);
        solution = result1 != null ? result1.moves : null;
    }            // find a solution to the initial board (using the A* algorithm)

    private final static RichBoard tryNeigbours(MinPQ<RichBoard> queue) {
        RichBoard min = queue.delMin();
        if (min.delegate.isGoal()) return min;
        for (Board neigbor : min.delegate.neighbors()) {
            if (min.moves.size() > 1 && neigbor.equals(min.moves.get(min.moves.size() - 2))) continue;
            queue.insert(new RichBoard(neigbor, concat(min.moves, neigbor)));
        }
        return null;
    }

    public boolean isSolvable()             {
        return solution != null;
    }// is the initial board solvable?

    public int moves()                      {
        return isSolvable() ? solution.size() - 1 : -1;
    }// min number of moves to solve initial board; -1 if no solution

    public Iterable<Board> solution()       {
        return solution;
    }// sequence of boards in a shortest solution; null if no solution

    public static void main(String[] args)  {
        // create initial board from file
        In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }// solve a slider puzzle (given below)

    private static final class RichBoard implements Comparable<RichBoard> {
        final Board delegate;
        final List<Board> moves;
        RichBoard(Board board, List<Board> moves) {
            this.delegate = board;
            this.moves = moves;
        }

        int moves() {
            return moves.size();
        }

        public Board delegate() {
            return delegate;
        }
        @Override
        public int compareTo(RichBoard o) {
            int x = delegate.manhattan() + moves.size();
            int y = o.delegate.manhattan() + o.moves.size();
            return (x < y) ? -1 : ((x == y) ? 0 : 1);
        }
    }

    private static <T> List<T> concat(List<T> list, T elem) {
        List<T> returnList = new ArrayList<T>(list);
        returnList.add(elem);
        return returnList;
    }
}