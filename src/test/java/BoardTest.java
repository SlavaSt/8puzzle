/* 4/6/14 */

import org.junit.Test;

import java.util.AbstractMap;

import static org.junit.Assert.assertEquals;

/**
 * @author Slava Stashuk
 */
public class BoardTest {
    @Test
    public void testManhattan() throws Exception {

        int[][] values = {
                {8, 1, 3},
                {4, 0, 2},
                {7, 6, 5}
        };

        Board board = new Board(values);

        assertEquals(10, board.manhattan());

    }

    @Test
    public void testNumberToCoords() {
        int[][] values = {
                {8, 1, 3},
                {4, 0, 2},
                {7, 6, 5}
        };

        Board board = new Board(values);

//        assertEquals(new AbstractMap.SimpleImmutableEntry(0, 0), board.numberToCoords(1));
//        assertEquals(new AbstractMap.SimpleImmutableEntry(0, 1), board.numberToCoords(2));
//        assertEquals(new AbstractMap.SimpleImmutableEntry(0, 2), board.numberToCoords(3));
//
//        assertEquals(new AbstractMap.SimpleImmutableEntry(1, 0), board.numberToCoords(4));
//        assertEquals(new AbstractMap.SimpleImmutableEntry(1, 1), board.numberToCoords(5));
//        assertEquals(new AbstractMap.SimpleImmutableEntry(1, 2), board.numberToCoords(6));
//
//        assertEquals(new AbstractMap.SimpleImmutableEntry(2, 0), board.numberToCoords(7));
//        assertEquals(new AbstractMap.SimpleImmutableEntry(2, 1), board.numberToCoords(8));
//        assertEquals(new AbstractMap.SimpleImmutableEntry(2, 2), board.numberToCoords(0));
    }
}
